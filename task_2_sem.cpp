#include <pthread.h>
#include <semaphore.h>
#include <cstdio>
#include <cstdlib>

struct SharedData {
	int inputNumber;
	int outputNumber;
	bool quit;
	sem_t readyForInputSem;
	sem_t inputReadySem;
	sem_t readyForOutputSem;
	sem_t outputReadySem;
};

void* inputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	char inputBuffer[16];
	while (data->quit == false) {
		if (std::fgets(inputBuffer, sizeof(inputBuffer), stdin) != nullptr) {
			if (inputBuffer[0] == 'q') {
				sem_wait(&data->readyForInputSem);
				data->quit = true;
				sem_post(&data->inputReadySem);
			} else {
				int number = std::atoi(inputBuffer);
				if (number != 0) {
					sem_wait(&data->readyForInputSem);
					data->inputNumber = number;
					printf("inputFunc sending %i\n", data->inputNumber);
					sem_post(&data->inputReadySem);
				}
			}
		}
	}

	printf("inputFunc quitting\n");

	pthread_exit(nullptr);
}

void* workFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	while (data->quit == false) {
		sem_post(&data->readyForInputSem);
		sem_wait(&data->inputReadySem);
		
		sem_wait(&data->readyForOutputSem);

		if (data->quit == false) {
			data->outputNumber = data->inputNumber * 3;
			printf("workFunc turning %i into %i\n", data->inputNumber, data->outputNumber);
		}

		sem_post(&data->outputReadySem);
	}

	printf("workFunc quitting\n");

	pthread_exit(nullptr);
}

void* outputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	while (data->quit == false) {
		sem_post(&data->readyForOutputSem);
		sem_wait(&data->outputReadySem);

		if (data->quit == false) {
			printf("outputFunc printing %i\n", data->outputNumber);
		}
	}

	printf("outputFunc quitting\n");

	pthread_exit(nullptr);
}

int main(int argc, char* argv[])
{
	SharedData data;
	data.quit = false;

	sem_init(&data.readyForInputSem, 0, 0);
	sem_init(&data.inputReadySem, 0, 0);
	sem_init(&data.readyForOutputSem, 0, 0);
	sem_init(&data.outputReadySem, 0, 0);

	pthread_t inputThread;
	pthread_create(&inputThread, nullptr, &inputFunc, (void*)&data);

	pthread_t workThread;
	pthread_create(&workThread, nullptr, &workFunc, (void*)&data);

	pthread_t outputThread;
	pthread_create(&outputThread, nullptr, &outputFunc, (void*)&data);

	pthread_join(inputThread, nullptr);
	pthread_join(workThread, nullptr);
	pthread_join(outputThread, nullptr);

	sem_destroy(&data.readyForInputSem);
	sem_destroy(&data.inputReadySem);
	sem_destroy(&data.readyForOutputSem);
	sem_destroy(&data.outputReadySem);
}

#include <pthread.h>
#include <cstdlib>
#include <cstdio>

struct SharedData {
	int inputNumber;
	int outputNumber;
	bool quit;
	pthread_mutex_t inputMutex;
	pthread_mutex_t outputMutex;
	pthread_cond_t inputCondition;
	pthread_cond_t outputCondition;
};

void* inputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	char inputBuffer[16];
	while (data->quit == false) {
		if (std::fgets(inputBuffer, sizeof(inputBuffer), stdin) != nullptr) {
			if (inputBuffer[0] == 'q') {
				pthread_mutex_lock(&data->inputMutex);
				data->quit = true;
				pthread_cond_signal(&data->inputCondition);
				pthread_mutex_unlock(&data->inputMutex);
			} else {
				int number = std::atoi(inputBuffer);
				if (number != 0) {
					pthread_mutex_lock(&data->inputMutex);
					data->inputNumber = number;
					printf("inputFunc sending %i\n", data->inputNumber);
					pthread_cond_signal(&data->inputCondition);
					pthread_mutex_unlock(&data->inputMutex);
				}
			}

		}
	}

	printf("inputFunc quitting\n");

	pthread_exit(nullptr);
}

void* workFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;
		
	pthread_mutex_lock(&data->inputMutex);

	while (data->quit == false) {
		pthread_cond_wait(&data->inputCondition, &data->inputMutex);
		pthread_mutex_lock(&data->outputMutex);
	
		if (data->quit == false) {
			data->outputNumber = data->inputNumber * 3;
			printf("workFunc turning %i into %i\n", data->inputNumber, data->outputNumber);
		}

		pthread_cond_signal(&data->outputCondition);
		pthread_mutex_unlock(&data->outputMutex);
	}
	
	pthread_mutex_unlock(&data->inputMutex);
	
	
	printf("workFunc quitting\n");
	
	pthread_exit(nullptr);
}

void* outputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;
	
	pthread_mutex_lock(&data->outputMutex);
	
	while (data->quit == false) {
		pthread_cond_wait(&data->outputCondition, &data->outputMutex);

		if (data->quit == false) {
			printf("outputFunc printing %i\n", data->outputNumber);
		}
	}
	
	pthread_mutex_unlock(&data->outputMutex);

	printf("outputFunc quitting\n");

	pthread_exit(nullptr);
}

int main(int argc, char* argv[])
{
	SharedData data;
	data.quit = false;

	pthread_mutex_init(&data.inputMutex, nullptr);
	pthread_mutex_init(&data.outputMutex, nullptr);
	pthread_cond_init(&data.inputCondition, nullptr);
	pthread_cond_init(&data.outputCondition, nullptr);

	pthread_t inputThread;
	pthread_create(&inputThread, nullptr, &inputFunc, (void*)&data);
	
	pthread_t workThread;
	pthread_create(&workThread, nullptr, &workFunc, (void*)&data);
	
	pthread_t outputThread;
	pthread_create(&outputThread, nullptr, &outputFunc, (void*)&data);

	pthread_join(inputThread, nullptr);
	pthread_join(workThread, nullptr);
	pthread_join(outputThread, nullptr);
	
	pthread_mutex_destroy(&data.inputMutex);
	pthread_mutex_destroy(&data.outputMutex);
	pthread_cond_destroy(&data.inputCondition);
	pthread_cond_destroy(&data.outputCondition);
}

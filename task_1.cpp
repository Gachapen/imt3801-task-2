#include <chrono>
#include <random>
#include <pthread.h>
#include <cstdio>

const int NUM_THREADS = 4;
const int NUM_SAMPLES = 30000000;

unsigned int monteCarloHits[NUM_THREADS];

void* monteCarloPiWorker(void* userData)
{
	const unsigned int THREAD_NUMBER = *((unsigned int*)userData);

	std::random_device randomDevice;
	std::default_random_engine randomEngine(randomDevice());
	std::uniform_real_distribution<double> numberDistribution(0.0, 1.0);

	const size_t NUM_RUNS = NUM_SAMPLES / NUM_THREADS;
	for (size_t i = 0; i < NUM_RUNS; i++)
	{
		double x = numberDistribution(randomEngine);
		double y = numberDistribution(randomEngine);
		if ((x * x) + (y * y) <= 1.0) {
			monteCarloHits[THREAD_NUMBER]++;
		}
	}

	pthread_exit(nullptr);
}

int main(int argc, char* argv[])
{
	auto startTime = std::chrono::high_resolution_clock::now();

	pthread_t threads[NUM_THREADS];
	unsigned int threadNumber[NUM_THREADS];

	for (size_t i = 0; i < NUM_THREADS; i++) {
		threadNumber[i] = i;
		monteCarloHits[i] = 0;
		pthread_create(&threads[i], nullptr, &monteCarloPiWorker, (void*)&threadNumber[i]);
	}

	int totalHits = 0;
	
	for (size_t i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], nullptr);
		totalHits += monteCarloHits[i];
	}

	double pi = ((double)totalHits / (double)NUM_SAMPLES) * 4.0;

	auto endTime = std::chrono::high_resolution_clock::now();
	double runTime = (std::chrono::duration<double>(endTime - startTime)).count();
	
	printf("%i pthreads approximated %.4f as pi with %d samples in %.4f seconds.\n", NUM_THREADS, pi, NUM_SAMPLES, runTime);
}

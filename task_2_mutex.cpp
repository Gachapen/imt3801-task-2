#include <thread>
#include <chrono>
#include <pthread.h>
#include <cstdio>

struct SharedData {
	int inputNumber;
	int outputNumber;
	bool quit;
	pthread_mutex_t inputMutex;
	pthread_mutex_t outputMutex;
};

void* inputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	char inputBuffer[16];
	while (data->quit == false) {
		if (std::fgets(inputBuffer, sizeof(inputBuffer), stdin) != nullptr) {
			if (inputBuffer[0] == 'q') {
				pthread_mutex_lock(&data->inputMutex);
				data->quit = true;
				pthread_mutex_unlock(&data->inputMutex);
			} else {
				int number = std::atoi(inputBuffer);
				if (number != 0) {
					pthread_mutex_lock(&data->inputMutex);
					data->inputNumber = number;
					printf("inputFunc sending %i\n", data->inputNumber);
					pthread_mutex_unlock(&data->inputMutex);
				}
			}
		}
	}

	printf("inputFunc quitting\n");

	pthread_exit(nullptr);
}

void* workFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	while (data->quit == false) {
		std::this_thread::sleep_for(std::chrono::milliseconds(5));

		pthread_mutex_lock(&data->inputMutex);
		pthread_mutex_lock(&data->outputMutex);
	
		if (data->quit == false && data->inputNumber != 0) {
			data->outputNumber = data->inputNumber * 3;
			printf("workFunc turning %i into %i\n", data->inputNumber, data->outputNumber);
			data->inputNumber = 0;
		}

		pthread_mutex_unlock(&data->outputMutex);
		pthread_mutex_unlock(&data->inputMutex);
	}
	
	printf("workFunc quitting\n");
	
	pthread_exit(nullptr);
}

void* outputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;
	
	while (data->quit == false) {
		std::this_thread::sleep_for(std::chrono::milliseconds(5));

		pthread_mutex_lock(&data->outputMutex);

		if (data->quit == false && data->outputNumber != 0) {
			printf("outputFunc printing %i\n", data->outputNumber);
			data->outputNumber = 0;
		}
	
		pthread_mutex_unlock(&data->outputMutex);
	}
	
	printf("outputFunc quitting\n");

	pthread_exit(nullptr);
}

int main(int argc, char* argv[])
{
	SharedData data;
	data.quit = false;

	pthread_mutex_init(&data.inputMutex, nullptr);
	pthread_mutex_init(&data.outputMutex, nullptr);

	pthread_t inputThread;
	pthread_create(&inputThread, nullptr, &inputFunc, (void*)&data);
	
	pthread_t workThread;
	pthread_create(&workThread, nullptr, &workFunc, (void*)&data);
	
	pthread_t outputThread;
	pthread_create(&outputThread, nullptr, &outputFunc, (void*)&data);

	pthread_join(inputThread, nullptr);
	pthread_join(workThread, nullptr);
	pthread_join(outputThread, nullptr);
	
	pthread_mutex_destroy(&data.inputMutex);
	pthread_mutex_destroy(&data.outputMutex);
}

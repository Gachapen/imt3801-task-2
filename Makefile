CC = g++
CFLAGS = -pthread -Wall -std=c++11

task_1: task_1.cpp
	$(CC) $(CFLAGS) -o $@ $<

task_2_cond: task_2_cond.cpp
	$(CC) $(CFLAGS) -o $@ $<

task_2_barrier: task_2_barrier.cpp
	$(CC) $(CFLAGS) -o $@ $<

task_2_sem: task_2_sem.cpp
	$(CC) $(CFLAGS) -o $@ $<

task_2_mutex: task_2_mutex.cpp
	$(CC) $(CFLAGS) -o $@ $<

task_2: task_2_cond task_2_barrier task_2_sem task_2_mutex

all: task_1 task_2

clean:
	rm -f task_1 task_2_cond task_2_barrier task_2_sem task_2_mutex
	

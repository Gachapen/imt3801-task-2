#include <pthread.h>
#include <cstdio>
#include <cstdlib>

struct SharedData {
	int inputNumber;
	int outputNumber;
	bool quit;
	pthread_barrier_t gettingInputBarrier;
	pthread_barrier_t inputDoneBarrier;
	pthread_barrier_t convertNumberBarrier;
	pthread_barrier_t outputReadyBarrier;
};

void* inputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;

	char inputBuffer[16];
	while (data->quit == false) {
		if (std::fgets(inputBuffer, sizeof(inputBuffer), stdin) != nullptr) {
			if (inputBuffer[0] == 'q') {
				pthread_barrier_wait(&data->gettingInputBarrier);
				data->quit = true;
				pthread_barrier_wait(&data->inputDoneBarrier);
			} else {
				int number = std::atoi(inputBuffer);
				if (number != 0) {
					// This will block the input until workFunc is ready. Should it
					// rather just miss an input if it is not ready?
					pthread_barrier_wait(&data->gettingInputBarrier);
					data->inputNumber = number;
					printf("inputFunc sending %i\n", data->inputNumber);
					pthread_barrier_wait(&data->inputDoneBarrier);
				}
			}
		}
	}

	printf("inputFunc quitting\n");

	pthread_exit(nullptr);
}

void* workFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;
	bool quit = false;

	while (quit == false) {
		pthread_barrier_wait(&data->gettingInputBarrier);
		pthread_barrier_wait(&data->inputDoneBarrier);

		// Again this will block when outputFunc is not ready. Should
		// it rather just miss an output if it is not ready?
		pthread_barrier_wait(&data->convertNumberBarrier);

		if (data->quit == true) {
			quit = true;
		} else {
			data->outputNumber = data->inputNumber * 3;
			printf("workFunc turning %i into %i\n", data->inputNumber, data->outputNumber);
		}

		pthread_barrier_wait(&data->outputReadyBarrier);
	}
	
	printf("workFunc quitting\n");
	
	pthread_exit(nullptr);
}

void* outputFunc(void* userData)
{
	SharedData* data = (SharedData*)userData;
	bool quit = false;
	
	while (quit == false) {
		pthread_barrier_wait(&data->convertNumberBarrier);
		pthread_barrier_wait(&data->outputReadyBarrier);

		if (data->quit == true) {
			quit = true;
		} else {
			printf("outputFunc printing %i\n", data->outputNumber);
		}
	}
	
	printf("outputFunc quitting\n");

	pthread_exit(nullptr);
}

int main(int argc, char* argv[])
{
	SharedData data;
	data.quit = false;

	pthread_barrier_init(&data.gettingInputBarrier, nullptr, 2);
	pthread_barrier_init(&data.inputDoneBarrier, nullptr, 2);
	pthread_barrier_init(&data.convertNumberBarrier, nullptr, 2);
	pthread_barrier_init(&data.outputReadyBarrier, nullptr, 2);

	pthread_t inputThread;
	pthread_create(&inputThread, nullptr, &inputFunc, (void*)&data);
	
	pthread_t workThread;
	pthread_create(&workThread, nullptr, &workFunc, (void*)&data);
	
	pthread_t outputThread;
	pthread_create(&outputThread, nullptr, &outputFunc, (void*)&data);

	pthread_join(inputThread, nullptr);
	pthread_join(workThread, nullptr);
	pthread_join(outputThread, nullptr);
	
	pthread_barrier_destroy(&data.gettingInputBarrier);
	pthread_barrier_destroy(&data.inputDoneBarrier);
	pthread_barrier_destroy(&data.convertNumberBarrier);
	pthread_barrier_destroy(&data.outputReadyBarrier);
}
